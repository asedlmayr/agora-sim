"""This profile instantiates a machine running Ubuntu 20.04 64-bit LTS with 2 40Gbps NICs and allocates all existing disk space on it. The machine is connected to a Skylark FAROS massive MIMO system comprised of a FAROS hub, a Faros massive MIMO Base Station.  A second d740 machine is running the same base Ubuntu 20.04 image and is connected to a set of Iris UEs (clients).  Additionally, the image has Intel oneapi, Flexran, and all other compilation dependencies of Agora preinstalled as an image backed dataset.

Since this profile includes multiple disk images it will can take up to 30 minutes to instantiate

An optional MATLAB installation is useful to run experiments on FAROS with RENEWLab demos. 

Instructions:
Do all your work (compile / running) from the /scratch directory.
sudo chsh -s /bin/bash YOURUSERNAME

- For more information on Agora please refer to our [github](https://github.com/jianding17/Agora/wiki) and [wiki](https://github.com/jianding17/Agora) sites.
- For more information on RENEWLab, see [RENEW documentation page](https://wiki.renew-wireless.org/)

"""

import geni.portal as portal
import geni.urn as urn
import geni.rspec.pg as pg
import geni.rspec.emulab as elab
import geni.rspec.emulab.spectrum as spectrum

# Resource strings
#PCIMG = 'urn:publicid:IDN+emulab.net+image+argos-test:agora-2004-base'
MATLAB_DS_URN = 'urn:publicid:IDN+emulab.net:powdersandbox+imdataset+matlab2021ra-etc'
#INTEL_LIBS_URN = 'urn:publicid:IDN+emulab.net:argos-test+imdataset+oneapi-flexran21'
RENEW_DS_URN = 'urn:publicid:IDN+emulab.net:mww2023+imdataset+mww23-datasets'

PCIMG = 'urn:publicid:IDN+emulab.net+image+mww2023:renew-ubuntu-base'
INTEL_LIBS_URN = 'urn:publicid:IDN+emulab.net:mww2023+imdataset+oneapi-flexran-dpdk'

MATLAB_MP = "/usr/local/MATLAB"
STARTUP_SCRIPT = "/local/repository/faros_start.sh"
DEF_REMDS_MP = "/opt/data"

REMDS_TYPES = [("readonly", "Read Only"),
               ("rwclone", "Read-Write Clone (not persistent)"),
               ("readwrite", "Read-Write (persistent)")]

PC_HWTYPE_SEL = [("d430", "D430 - Min"),
                 ("d740", "D740 - Mid"),
                 ("d840", "D840 - Max")]

#
# Profile parameters.
#
pc = portal.Context()

pc.defineParameter("matlabds", "Attach the Matlab to the compute node (Optional).",
                   portal.ParameterType.BOOLEAN, True, advanced=True)

pc.defineParameter("pchwtype", "PC Hardware Type",
                   portal.ParameterType.IMAGE,
                   PC_HWTYPE_SEL[0], PC_HWTYPE_SEL, advanced=True,
                   longDescription="Select the PC Hardware Type For the Agora Simulator")

pc.defineParameter("pchwid", "Fixed PC1 Node_id (Optional)",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Fix 'pc1' to this specific node.  Leave blank to allow for any available node of the specified pchwtype")

#intel oneapi libs
pc.defineParameter("intellibs", "Attach the intel and 3rd party library dataset to the compute host.",
                   portal.ParameterType.BOOLEAN, True, advanced=True)

pc.defineParameter("intelmountpt", "Mountpoint for 3rd party libraries and inteloneAPI",
                   portal.ParameterType.STRING, "/opt", advanced=True)

pc.defineParameter("INTEL_LIBS_URN", "URN of the 3rd party library dataset", 
                   portal.ParameterType.STRING,
                   INTEL_LIBS_URN, advanced=True)
#renew dataset
pc.defineParameter("addrenewds", "Attach a prerecorded renew dataset.",
                   portal.ParameterType.BOOLEAN, True, advanced=True)

pc.defineParameter("renewdsmountpt", "Mountpoint for pre-recorded renew dataset.",
                   portal.ParameterType.STRING, "/renew_dataset", advanced=True)

pc.defineParameter("RENEW_DS_URN", "URN of the renew dataset", 
                   portal.ParameterType.STRING,
                   RENEW_DS_URN, advanced=True)

#remote dataset
pc.defineParameter("remds", "Remote Dataset (Optional)",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Insert URN of a remote dataset to mount. Leave blank for no dataset.")

pc.defineParameter("remmp", "Remote Dataset Mount Point (Optional)",
                   portal.ParameterType.STRING, DEF_REMDS_MP, advanced=True,
                   longDescription="Mount point for optional remote dataset.  Ignored if the 'Remote Dataset' parameter is not set.")

pc.defineParameter("remtype", "Remote Dataset Mount Type (Optional)",
                   portal.ParameterType.STRING, REMDS_TYPES[0], REMDS_TYPES,
                   advanced=True, longDescription="Type of mount for remote dataset.  Ignored if the 'Remote Dataset' parameter is not set.")

# Bind and verify parameters.
params = pc.bindParameters()
pc.verifyParameters()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Mount a remote dataset
def connect_DS(node, urn, mp, dsname = "", dstype = "rwclone"):
    if not dsname:
        dsname = "ds-%s" % node.name
    bs = request.RemoteBlockstore(dsname, mp)
    if dstype == "rwclone":
        bs.rwclone = True
    elif dstype == "readonly":
        bs.readonly = True
        
    # Set dataset URN
    bs.dataset = urn

    # Create link from node to dataset rw clone
    bslink = request.Link("link_%s" % dsname, members=(node, bs.interface))
    bslink.vlan_tagging = True
    bslink.best_effort = True

# Request PC1
pc1 = request.RawPC("pc1")

# Declare that you will be starting X11 VNC onpc1.
request.initVNC()

# Install and start X11 VNC. Calling this informs the Portal that you want a VNC
# option in the node context menu to create a browser VNC client.
pc1.startVNC()

if params.pchwid:
    pc1.component_id=params.pchwid
else:
    pc1.hardware_type = params.pchwtype
pc1.disk_image = PCIMG

if params.intellibs:
    ilbspc1 = pc1.Blockstore( "intellibbspc1", params.intelmountpt )
    ilbspc1.dataset = params.INTEL_LIBS_URN
    ilbspc1.placement = "sysvol"

if params.matlabds:
    mlbs = pc1.Blockstore( "matlabpc1", MATLAB_MP )
    mlbs.dataset = MATLAB_DS_URN
    mlbs.placement = "nonsysvol"

pc1.addService(pg.Execute(shell="sh", command="sudo chmod 775 /local/repository/faros_start.sh"))
pc1.addService(pg.Execute(shell="sh", command=STARTUP_SCRIPT))
#if1pc1 = pc1.addInterface("if1pc1", pg.IPv4Address("192.168.1.1", "255.255.255.0"))
#if1pc1.bandwidth = 40 * 1000 * 1000 # 40 Gbps
#if1pc1.latency = 0

if params.addrenewds:
    renewdatasetspc1 = pc1.Blockstore( "renewdatasets", params.renewdsmountpt )
    renewdatasetspc1.dataset = params.RENEW_DS_URN
    renewdatasetspc1.size = "4GB"
    renewdatasetspc1.readonly = True
    renewdatasetspc1.placement = "nonsysvol"

bss1 = pc1.Blockstore("pc1scratch","/scratch")
bss1.size = "100GB"
bss1.placement = "nonsysvol"
if params.remds:
    connect_DS(pc1, params.remds, params.remmp, dstype=params.remtype)

# Print the RSpec to the enclosing page.
pc.printRequestRSpec()
