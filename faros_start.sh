#!/bin/bash

SCRATCH="/scratch"

# Only run 1 time
STARTUP_FILE=$SCRATCH/.startup_complete.me
if [ -f "$STARTUP_FILE" ]; then
    exit 0
fi

MYWD=`dirname $0`
AGORAREPO="https://github.com/Agora-wireless/Agora.git"
RENEWLAB="https://github.com/renew-wireless/RENEWLab"

#Check to see if the mounts happened correctly
#/etc/fstab waiting here (/usr/local/matlab && /scratch/ && /renew_dataset/ would be best to pass these paths into the script
loop_ctr=0
while [ $loop_ctr -lt 20 -a $(grep -csi scratch /etc/fstab) -eq 0 ]
do
  echo "scratch directory does not exist yet"
  sleep 30
  loop_ctr=`expr $loop_ctr + 1`
done

if [ $(grep -csi scratch /etc/fstab) -qe 0 ]
then
  echo "Scratch does not exist"
  exit 4
fi

if [ $(grep -csi opt /etc/fstab) -qe 0 ]
then
  echo "Opt dir does not exist"
  exit 5
fi

if [ $(grep -csi renew_dataset /etc/fstab) -qe 0 ]
then
  echo "Renew Dataset does not exist"
  exit 6
fi

#Fix in case package name changes
sudo apt-get -y update --allow-releaseinfo-change

#Add symlink to datasets
sudo ln -s /renew_dataset /scratch/renew_dataset

cd $SCRATCH
sudo chown ${USER}:${GROUP} .
sudo chmod 775 .

mkdir dependencies
mkdir repos
cd repos

git clone --branch mww23-updates $AGORAREPO agora || \
    { echo "Failed to clone git repository: $AGORAREPO" && exit 1; }

git clone --branch mww-config $RENEWLAB RENEWLab || \
    { echo "Failed to clone git repository: $RENEWLAB" && exit 1; }

cd ../dependencies

# --- Armadillo (11.4.2)
wget http://sourceforge.net/projects/arma/files/armadillo-11.4.2.tar.xz
tar -xf armadillo-11.4.2.tar.xz
cd armadillo-11.4.2
cmake -DALLOW_OPENBLAS_MACOS=ON .
make -j`nproc`
sudo make install
sudo ldconfig
cd ../

# Install Soapy tools
# SoapySDR
git clone --branch soapy-sdr-0.8.1 --depth 1 --single-branch https://github.com/pothosware/SoapySDR.git
cd SoapySDR
mkdir -p build
cd build
cmake ../
make -j`nproc`
sudo make install
cd ../..
sudo ldconfig

#SoapyRemote
git clone --branch soapy-remote-0.5.2 --depth 1 --single-branch https://github.com/pothosware/SoapyRemote.git
cd SoapyRemote
mkdir -p build
cd build
cmake ../
make -j`nproc`
sudo make install
cd ../..
sudo ldconfig

#Iris drivers
git clone --branch bypass-socket-mod --depth 1 --single-branch https://github.com/Agora-wireless/sklk-soapyiris.git
cd sklk-soapyiris
mkdir -p build
cd build
cmake ../
make -j`nproc`
sudo make install
cd ../..
sudo ldconfig

source /opt/intel/oneapi/setvars.sh --config="/opt/intel/oneapi/renew-config.txt"

#Enable the soapy sdr server for more robust device detection
sudo systemctl enable SoapySDRServer

# Install FlexRAN FEC SDK
cd /opt/FlexRAN-FEC-SDK-19-04/sdk
sudo rm -rf build-avx*

WIRELESS_SDK_TARGET_ISA="avx2"
export WIRELESS_SDK_TARGET_ISA
./create-makefiles-linux.sh
cd build-avx2-icc
make -j
sudo make install
sudo ldconfig

WIRELESS_SDK_TARGET_ISA="avx512"
export WIRELESS_SDK_TARGET_ISA
./create-makefiles-linux.sh
cd build-avx512-icc
make -j
sudo make install
sudo ldconfig

#Make the saopy settings a lower priority
sudo mv /usr/local/lib/sysctl.d/SoapySDRServer.conf /usr/local/lib/sysctl.d/98-SoapySDRServer.conf
#Ethernet buffer sizes
echo -e '# Ethernet transport tuning\n# Socket Rx Buffer Max Size\nnet.core.rmem_max=536870912\n#Socket Send Buffer Max Size\nnet.core.wmem_max=536870912' | sudo tee /etc/sysctl.d/99-agora.conf
sudo sysctl --load /etc/sysctl.d/99-agora.conf

echo -e '#!/usr/bin/bash\nsource /opt/intel/oneapi/setvars.sh --config="/opt/intel/oneapi/renew-config.txt"' | sudo tee /etc/profile.d/10-inteloneapivars.sh
#non-login consoles
echo -e '\n#Gen intel env vars\nsource /opt/intel/oneapi/setvars.sh --config="/opt/intel/oneapi/renew-config.txt"' | sudo tee -a /etc/bash.bashrc
echo -e '#!/usr/bin/bash\nsudo chown -R $(id -u):$(id -g) /scratch/ > /dev/null 2>&1' | sudo tee /etc/profile.d/11-scratchowner.sh
#non-login consoles
echo -e '#Set user in control of working dir\nsudo chown -R $(id -u):$(id -g) /scratch/ > /dev/null 2>&1' | sudo tee -a /etc/bash.bashrc

#Build Agora
cd $SCRATCH/repos/agora
mkdir build
cd build
cmake .. -DRADIO_TYPE=SIMULATION
make -j
cd $SCRATCH

#Modify the grub file to isolate the cpu cores turn off multithreading, cpu mitigations, and sets hugepage support, iommu enabled for dpdk vfio.
global_options="default_hugepagesz=1G hugepagesz=1G hugepages=4 mitigations=off nosmt intel_iommu=on iommu=pt"
#d840 specific cpu setup
isolcpus_d840="isolcpus=1-3,5-7,9-11,13-15,17-19,21-23,25-27,29-31,33-35,37-39,41-43,45-47,49-51,53-55,57-59,61-63"
irqaffinity_d840="irqaffinity=0,4,8,12,16,20,24,28,32,36,40,44,48,52,56,60"
#d740 specific cpu setup
isolcpus_d740="isolcpus=1,3,5,7,9,11,13,15,17,19,21,23"
irqaffinity_d740="irqaffinity=0,2,4,6,8,10,12,14,16,18,20,22"
#d430 specific cpu setup
isolcpus_d430="isolcpus=1,3,5,7,9,11,13,15"
irqaffinity_d430="irqaffinity=0,2,4,6,8,10,12,14"

#Determine the correct configuration
cpu_count=`nproc --all`
if [ $cpu_count -eq 64 ]; then
  echo "Detected D840"
  isolcpus=$isolcpus_d840
  irqaffinity=$irqaffinity_d840
elif [ $cpu_count -eq 24 ]; then
  echo "Detected D740"
  isolcpus=$isolcpus_d740
  irqaffinity=$irqaffinity_d740
elif [ $cpu_count -eq 16 ]; then
  echo "Detected D430"
  isolcpus=$isolcpus_d430
  irqaffinity=$irqaffinity_d430
else
  echo "CPU TYPE NOT SUPPORTED.  Found $cpu_count CPUS - Defaulting to d430"
  isolcpus=$isolcpus_d430
  irqaffinity=$irqaffinity_d430
fi

sudo sed -i "s/GRUB_CMDLINE_LINUX_DEFAULT.*/GRUB_CMDLINE_LINUX_DEFAULT=\"$global_options $isolcpus $irqaffinity\"/1" /etc/default/grub
sudo update-grub

# Add startup file so we don't run again
touch $STARTUP_FILE

sudo shutdown -r +1 "Rebooting with modified kernel parameters"
exit $?
